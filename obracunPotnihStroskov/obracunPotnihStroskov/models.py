from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

from django.db import models


class uporabnik(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    role = models.CharField(max_length=100)


class travelOrder(models.Model):
    user = models.ForeignKey(uporabnik, on_delete=models.CASCADE)
    dateOfTravel = models.DateTimeField('date deleted')
    days = models.IntegerField(default=0)
    destination = models.CharField(max_length=100)
    opened = models.BooleanField(default=False)

class advance(models.Model):
    travelOrder = models.ForeignKey(travelOrder, on_delete=models.CASCADE)
    money = models.IntegerField(default=0)

class expence(models.Model):
    travelOrder = models.ForeignKey(travelOrder, on_delete=models.CASCADE)
    money = models.IntegerField(default=0)
