from django.http import HttpResponse
from .models import uporabnik
from django.template import loader
from django.shortcuts import render

def usi(request):
    return HttpResponse("Hello, world. You're at the polls index.")
def detail(request, userId):
    try:
        question = uporabnik.objects.get(pk=userId)
    except uporabnik.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'obracunPotnihStroskov/detail.html', {'detail': question})
def index(request):
    list=uporabnik.objects.all()
    context = {'latest_question_list': list}
    return render(request, 'obracunPotnihStroskov/index.html', context)
