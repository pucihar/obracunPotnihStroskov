Obracun potnih stroškov
Žena sestricnine tete Genovefa ima srednje veliko podjetje, kjer zaposleni dosti potujejo po razlicnih konferencah po svetu. Trenutno poteka proces obracuna stroškov povezanih s službeno potjo nekako takole:

Zaposleni pred odhodom izpolni obrazec za odobritev službene poti. Na obrazcu mora napisati kam in kdaj potuje. Oznaciti mora tudi ali želi za službeno pot prejeti predujem.
Njegov nadrejeni mora službeno pot v danih terminih potrditi (lahko jo seveda tudi zavrne in/ali predlaga spremembe).
Ce je pot odobrena, potem prejme zaposleni potni nalog.
Zaposleni vse (upravicene) stroške povezane s potjo dodaja na potni nalog.
Ko se zaposleni vrne odda potni nalog v racunovodstvo.
Racunovodstvo oddano pregleda in odobri ali zavrne posamezne stroške.
Odobreni stroški se zaposlenemu povrnejo na TRR. Pri vracilu stroškov je potrebno preveriti tudi ali je zaposleni že dobil predujem in seveda ustrezno vrednost odšteti.
Vaša spletna aplikacija naj podpre opisani proces. Posebno pozornost namenite dodajanju nastalih stroškov na potni nalog. Upoštevajte, da lahko dodajamo stroške prevoza (letalska vozovnica, taxi, rent-a-car, ...), stroške bivanja (hotel, motel, ...), stroške prehrane (to sicer zajema dnevnica, ampak ni treba komplicirati s tem). Pomislite tudi na to, da je za nastale stroške potrebno oddati dokazila (slika oz. scan racuna, vozovnice, ...).

Ne pozabite tudi, da imajo razlicni zaposleni razlicne vloge v procesu ustvarjanja, potrjevanja in obracuna potnega naloga.




------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
spletna aplikacija za podjetja, cigar zaposleni veliko potujejo.

imamo 3 razlicne uporabnike:
1.-oseba ki zaprosi za odobritev potnega naloga, ko je odobren ga izpolnjuje
2.-njegov nadrejeni, ki odobri ali zavrne njegovo prosnjo
3.-racunovodja ki odobri ali zavrne posamezne stroske iz potnega naloga, nakaze predujem, nakaze tudi ostale stroske

torej na zacetku bo page ki pritegne uporabnika, odprl se mu bo tudi dropdown za vpis, vpisal se bo kot eden izmed 3h uporabnikov
nato se bo za 1. odprla stran kjer bo moznost za:
- izpolnitev prosnje za nov potni nalog
- pregled ze odobrenih prosenj, ki jih lahko izpolnjuje
- pregled ze zakljucenih potnih stroskov (vpogled v arhiv svojih potnih stroskov)
za 2. se bo izpisala:
	-vse prosnje ki jih sprejme/zavrne
	-vse prosnje ki jih je kdajkoli odobril
	-vse osebe in njihove prosnje
za 3. se bo odprla:
	-stran kjer bos lahko izbiral med predujemi ki bi jih lahko izplazal
	-stran zakljucenih postnih stroskov ki jih je potrebno predelati
	-vse osebe in njihove prosnje
	
tezave so bile takorekoc povsod, saj sem se moral precej veliko nauciti :D
najbol zanimive tezave so bile z javascriptom(jih sploh se nisem odpravil) in slikami (chrome je prikazal vse slike, safari in mozzila pa nekaterih nista hotela)